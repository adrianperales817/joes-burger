var routerApp = angular.module('routerApp', ['ui.router']);

routerApp.config(function($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.otherwise('/home');
	
	$stateProvider
		.state ('home', {
			url:'/home',
			templateUrl:'Home.html'
		})
		
		.state ('menu', {
			url:'/good Eats',
			templateUrl:'menu.html'
		})
		
		.state ('about', {
			url:'/get to know us',
			templateUrl:'aboutUs.html'
		})
		
		.state ('contact', {
			url:'/holla',
			templateUrl:'Contact.html'
		});
	});
});